import { createStore, compose, applyMiddleware } from "redux";
import createSagaMiddleWare from "redux-saga";
import rootReducer from "./components/";
import { handleData } from "./components/table";

const sagaMiddleware = createSagaMiddleWare();

const createAppStore = () => {
  const store = createStore(
    rootReducer,
    compose(
      applyMiddleware(sagaMiddleware),
      window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : (noop) => noop
    )
  );

  sagaMiddleware.run(handleData);

  return store;
};

export default createAppStore;
