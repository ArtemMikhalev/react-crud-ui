import { handleActions } from "redux-actions";
import {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
} from "./actions";

export const data = handleActions(
  {
    [fetchDataRequest]: () => [],
    [fetchDataSuccess]: (_state, action) => action.payload,
  },
  []
);

export const isLoading = handleActions(
  {
    [fetchDataRequest]: () => true,
    [fetchDataSuccess]: () => false,
    [fetchDataFailure]: () => false,
  },
  false
);

export const error = handleActions(
  {
    [fetchDataRequest]: () => null,
    [fetchDataFailure]: (_state, action) => action.payload,
  },
  null
);
