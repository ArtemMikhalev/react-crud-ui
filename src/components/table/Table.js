import React, { useEffect } from "react";
import MaterialTable from "material-table";
import { Paper } from "@material-ui/core";
import { fetchDataRequest } from "./actions";
import { connect } from "react-redux";
import { getRecords, getIsLoading, getError } from "./selectors";

function getData(records) {
  let data = [];

  records.forEach((record) => {
    let row = {};
    row._id = record._id;
    for (let property in record.data) {
      row[property] = record.data[property];
    }
    data.push(row);
  });
  return data;
}

const TableElem = React.memo((props) => {
  useEffect(() => {
    const { fetchDataRequest } = props;
    fetchDataRequest();
  }, []);

  const { records, fetchDataRequest } = props;
  const columns = [
    { title: "ID", field: "_id", editable: "never" },
    { title: "Name", field: "name" },
    { title: "Surname", field: "surname" },
    { title: "Age", field: "age" },
  ];
  const data = getData(records);
  const { isLoading, error } = props;

  if (isLoading) return <p>Данные загружаются</p>;
  if (error)
    return <p>{`произошла сетевая ошибка ${error.name}: ${error.message}`}</p>;
  return (
    <Paper>
      <MaterialTable
        localization={{
          header: {
            actions: "Действия",
          },
          toolbar: {
            searchTooltip: "Поиск",
            searchPlaceholder: "Поиск",
          },
          body: {
            addTooltip: "Добавить",
            deleteTooltip: "Удалить",
            editTooltip: "Редактировать",
            emptyDataSourceMessage: "Нет данных для отображения",
            editRow: {
              saveTooltip: "Сохранить",
              cancelTooltip: "Отмена",
              deleteText: "Вы действительно хотите удалить запись?",
            },
          },
          pagination: {
            labelRowsSelect: "Записей",
            previousTooltip: "Предыдущая страница",
            nextTooltip: "Следующая таблица",
            firstTooltip: "Первая страница",
            lastTooltip: "Последняя страница",
          },
        }}
        title={"CRUD-UI table"}
        columns={columns}
        data={data}
        options={{
          pageSize: 10,
        }}
        editable={{
          onRowAdd: (newRow) =>
            new Promise((resolve, reject) => {
              fetch("http://178.128.196.163:3000/api/records/", {
                method: "PUT",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  data: newRow,
                }),
              })
                .then()
                .then(() => fetchDataRequest())
                .catch((error) => {
                  alert(error.message);
                  resolve();
                });
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              const body = {
                data: {
                  name: newData.name,
                  surname: newData.surname,
                  age: newData.age,
                },
              };
              fetch(`http://178.128.196.163:3000/api/records/${oldData._id}`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(body),
              })
                .then()
                .then(() => fetchDataRequest())
                .catch((error) => {
                  alert(error.message);
                  resolve();
                });
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve, reject) => {
              fetch(`http://178.128.196.163:3000/api/records/${oldData._id}`, {
                method: "DELETE",
              })
                .then()
                .then(() => {
                  fetchDataRequest();
                })
                .catch((error) => {
                  alert(error.message);
                  resolve();
                });
            }),
        }}
      />
    </Paper>
  );
});

const mapStateToProps = (state) => ({
  records: getRecords(state),
  isLoading: getIsLoading(state),
  error: getError(state),
});

const mapDispatchToProps = { fetchDataRequest };

const Table = connect(mapStateToProps, mapDispatchToProps)(TableElem);

export { Table };
