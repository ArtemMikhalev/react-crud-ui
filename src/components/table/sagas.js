import { takeEvery, call, put } from "redux-saga/effects";
import {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
} from "./actions";

const getData = () => {
  return fetch("http://178.128.196.163:3000/api/records").then((response) =>
    response.json()
  );
};

export function* handleData() {
  yield takeEvery(fetchDataRequest, function* () {
    try {
      const result = yield call(getData);
      yield put(fetchDataSuccess(result));
    } catch (error) {
      yield put(fetchDataFailure(error));
    }
  });
}
