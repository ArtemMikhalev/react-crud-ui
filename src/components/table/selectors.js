import { createSelector } from "reselect";

export const getRecords = createSelector(
  (state) => state.data,
  (records) => records
);

export const getIsLoading = createSelector(
  (state) => state.isLoading,
  (isLoading) => isLoading
);

export const getError = createSelector(
  (state) => state.error,
  (error) => error
);
