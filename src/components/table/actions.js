import { createAction } from "redux-actions";

export const fetchDataRequest = createAction("FETCH_DATA_REQUEST");
export const fetchDataSuccess = createAction("FETCH_DATA_SUCCESS");
export const fetchDataFailure = createAction("FETCH_DATA_FAILURE");
