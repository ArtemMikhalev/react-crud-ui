import { combineReducers } from "redux";
import { data, isLoading, error } from "./table";

export default combineReducers({
  data,
  isLoading,
  error,
});
