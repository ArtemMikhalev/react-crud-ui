import React from "react";
import { Table } from "./components/table";
import { Container } from "@material-ui/core";

const App = () => {
  return (
    <Container>
      <h2>CRUD-UI</h2>
      <Table />
    </Container>
  );
};

export default App;
